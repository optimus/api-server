CREATE DATABASE IF NOT EXISTS `server` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `server`;

RENAME TABLE `users`.`users` TO `server`.`users`;
DROP DATABASE `users`;

ALTER TABLE `server`.`users` ADD COLUMN `server` varchar(128) DEFAULT NULL AFTER `id`;
ALTER TABLE `server`.`users` ADD COLUMN `firstname` varchar(64) DEFAULT NULL AFTER `email`;
ALTER TABLE `server`.`users` ADD COLUMN `lastname` varchar(64) DEFAULT NULL AFTER `firstname`;

CREATE TABLE `allowed_origins` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `origin` varchar(256) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `authorizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `resource` varchar(128) DEFAULT NULL,
  `read` bit(1) NOT NULL DEFAULT b'0',
  `write` bit(1) NOT NULL DEFAULT b'0',
  `create` bit(1) NOT NULL DEFAULT b'0',
  `delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `modules` (
  `name` varchar(32) NOT NULL,
  `displayname` varchar(128) NOT NULL,
  `module` varchar(32) NOT NULL,
  `group` varchar(32) NOT NULL,
  `position` tinyint(3) unsigned DEFAULT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `install` varchar(128) DEFAULT NULL,
  `uninstall` varchar(128) DEFAULT NULL,
  `user_install` varchar(128) DEFAULT NULL,
  `user_uninstall` varchar(128) DEFAULT NULL,
  `admin_only` bit(1) NOT NULL DEFAULT b'0',
  `disabled` bit(1) NOT NULL DEFAULT b'0',
  `description` text DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `modules_groups` (
  `name` varchar(32) NOT NULL,
  `displayname` varchar(128) NOT NULL,
  `position` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `preferences` (
  `owner` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `module` varchar(32) NOT NULL DEFAULT '',
  `preference` varchar(32) NOT NULL DEFAULT '',
  `value` text DEFAULT NULL,
  PRIMARY KEY (`owner`,`user`,`module`,`preference`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `services` (
  `name` varchar(32) NOT NULL,
  `displayname` varchar(128) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `install` varchar(128) DEFAULT NULL,
  `uninstall` varchar(128) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `users_services` (
  `user` int(10) unsigned NOT NULL,
  `service` varchar(32) NOT NULL,
  PRIMARY KEY (`user`,`service`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `users_servers` (
  `user` int(10) unsigned NOT NULL,
  `server` varchar(128) NOT NULL,
  PRIMARY KEY (`user`,`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

REPLACE INTO `server`.`modules_groups` SET name="general", displayname="Général", position=1;
REPLACE INTO `server`.`modules_groups` SET name="tools", displayname="Outils", position=126;
REPLACE INTO `server`.`modules_groups` SET name="administration", displayname="Administration", position=127;

REPLACE INTO `server`.`modules` SET name="webmail", displayname="Webmail", `module`="allspark", `group`="general", position=1, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="files", displayname="Fichiers", `module`="allspark", `group`="general", position=2, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=0, description=null;

REPLACE INTO `server`.`modules` SET name="visioconference", displayname="Visioconférence", `module`="allspark", `group`="tools", position=1, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="vcard", displayname="Vcard", `module`="allspark", `group`="tools", position=2, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="keeweb", displayname="Keeweb", `module`="allspark", `group`="tools", position=3, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="help", displayname="Aide", `module`="allspark", `group`="tools", position=7, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;

REPLACE INTO `server`.`modules` SET name="server-create", displayname="Créer", `module`="allspark", `group`="administration", position=1, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="server-admin", displayname="Administrer", `module`="allspark", `group`="administration", position=2, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=1, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="my-account", displayname="Mon compte", `module`="allspark", `group`="administration", position=3, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="server-users", displayname="Utilisateurs", `module`="allspark", `group`="administration", position=4, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=1, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="server-backup", displayname="Backup", `module`="allspark", `group`="administration", position=5, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=1, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="server-status", displayname="Statut", `module`="allspark", `group`="administration", position=6, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=1, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="server-reboot", displayname="Redémarrage", `module`="allspark", `group`="administration", position=7, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=1, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="server-console", displayname="Console", `module`="allspark", `group`="administration", position=8, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=1, disabled=1, description=null;
