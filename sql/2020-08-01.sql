CREATE DATABASE IF NOT EXISTS `users` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `users`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` bit NOT NULL DEFAULT b'0',
  `admin` bit NOT NULL DEFAULT b'0',
  `email` varchar(128) NOT NULL UNIQUE,
  `password` varbinary(128) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
