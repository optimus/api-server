<?php

use optimus\JWT\JWT;
function auth()
{
	global $input, $sha_key;
	
	if (isset($_COOKIE['token']))
		$token = $_COOKIE['token'];
	else
	{
		http_response_code(401);
		die(json_encode(array("code" => 401, "message" => "Accès refusé - Token absent")));
	}

	try
	{
		$payload = (new JWT($sha_key, 'HS512', 3600, 10))->decode($token);
		$input->user = $payload['user'];
	}
	catch (Throwable $e)
	{
		http_response_code(401);
		die(json_encode(array("code" => 401, "message" => "Accès refusé - " . $e->getMessage())));
	}
}


function is_allowed_origin($allowed_origins = NULL)
{
	global $connection;
	if ($allowed_origins == NULL)
		$allowed_origins = $connection->query("SELECT origin FROM server.allowed_origins")->fetch(PDO::FETCH_NUM);
	$origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : $_SERVER['SERVER_NAME'];
	$origin_without_protocol = substr($origin,strpos($origin,'://')+3);
	if (is_array($allowed_origins) AND (in_array('*',$allowed_origins) OR in_array($origin_without_protocol,$allowed_origins) OR in_array('*.'.substr($origin_without_protocol,strpos($origin_without_protocol,'.')+1),$allowed_origins)))
		return true;
	else
		return false;
}


function allowed_origins_only($allowed_origins = NULL)
{
	global $connection;
	if ($allowed_origins == NULL)
		$allowed_origins = array_column($connection->query("SELECT origin FROM server.allowed_origins")->fetchAll(PDO::FETCH_NUM),0);
	if (!is_allowed_origin($allowed_origins))
	{
		http_response_code(403);
		die(json_encode(array("code" => 403, "message" => "Accès refusé - Les connexions depuis cette origine ne sont pas autorisées")));
	}
}


function is_admin($user_id)
{
	global $connection;
	$isadmin = $connection->prepare("SELECT `admin` FROM `server`.`users` WHERE id = :id AND status = 1");
	$isadmin->bindParam(':id', $user_id);
	$isadmin->execute();
	$isadmin = $isadmin->fetch(PDO::FETCH_ASSOC);
	if ($isadmin['admin'] == 1)
		return true;
	else
		return false;
}


function admin_only()
{
	global $input;
	if (!is_admin($input->user->id))
	{
		http_response_code(403);
		die(json_encode(array("code" => 403, "message" => "Accès refusé - Cette méthode est réservée aux administrateurs")));
	}
}


function exists($connection, $db, $table, $field, $value)
{
	$exists = $connection->prepare("SELECT " . $field . " FROM `" . $db . "`.`" . $table . "` WHERE " . $field . " = :" . $field);
	$exists->bindParam(':'.$field, $value);
	$exists->execute();
	if ($exists->rowCount() == 0)
		return false;
	else
		return true;
}


function validate($name, $value, $format, $required = false)
{
	global $input;

	if (isset($value) && $value!='' && isset($format))
		if ($format == 'topdomain' AND !preg_match("/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)([a-z0-9][a-z0-9-]*[a-z0-9])$/", $value)) // web.network
			$return = array("code" => 400, "message" => $name . ' : ' . $value . " n'est pas un domaine de premier niveau valide");
		else if ($format == 'domain' AND !preg_match("/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9])$/", $value)) // web.network  test.web.network
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un domaine valide");
		else if ($format == 'email' AND !preg_match("/^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]))$/", $value)) // test_6@test.network  test-8@web.test.network
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une adresse email valide");
		else if ($format == 'email_list' AND !preg_match("/^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]);?)+$/", $value)) // first@test.org;second@test.org;third@test.org
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une liste d'adresses email valide");
		else if ($format == 'origin' AND !preg_match("/^(\*)$|^(localhost|(\*\.)?([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z0-9][a-z0-9-]*[a-z0-9]|(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3})(:((6553[0-5])|(655[0-2][0-9])|(65[0-4][0-9]{2})|(6[0-4][0-9]{3})|([1-5][0-9]{4})|([0-5]{0,5})|([0-9]{1,4})))?$/", $value)) // *  localhost:8080  *.optimus-avocats.fr  www.optimus-avocats.fr:9091  192.168.0.5:3232
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une origine valide");
		else if ($format == 'ipv4' AND !preg_match("/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/", $value)) // 192.168.0.5
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une adresse ip valide");
		else if ($format == 'integer' AND !preg_match("/^-?\d+$/", $value)) // 1215 -4 0 2
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier valide");
		else if ($format == 'positive_integer' AND !preg_match("/^\d+$/", $value)) // 0 1215
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier positif valide");
		else if ($format == 'negative_integer' AND !preg_match("/^-\d+$/", $value)) // -1215 0
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier négatif valide");
		else if ($format == 'strictly_positive_integer' AND !preg_match("/^[1-9]\d*$/", $value)) // 1215
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier strictement positif valide");
		else if ($format == 'strictly_negative_integer' AND !preg_match("/^-[1-9]\d*$/", $value)) // -1215
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier strictement négatif valide");
		else if ($format == 'decimal' AND !preg_match("/^-?(\d*\.)?\d+$/", $value)) // 44.59
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre decimal valide");
		else if ($format == 'boolean' AND !preg_match("/^[01]$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre boolean valide. Valeurs autorisées : 0 ou 1");
		else if ($format == 'string' AND !preg_match("/^[\w\t \-œàâäéèêëïîôöùûüÿç().,?!\"\'+=+\-=\$\€\/_*@&~#%:;°§\[\]\{\}]+$/D", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une chaîne valide");
		else if ($format == 'text' AND !preg_match("/^[\w\t \-œàâäéèêëïîôöùûüÿç().,?!\"\'+=+\-=\£\$\€\/_*@&~#%:;°§\[\]\{\}]+$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une chaîne multiligne valide");
		else if ($format == 'filename' AND ($value=='.' OR $value=='..' OR substr($value,0,1)==' ' OR substr($value,-1)==' ' OR !preg_match("/^[a-zA-Z0-9 ._@\-àâäéèêëïîôöùûüÿç()\']+$/", $value)))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nom de fichier valide");
		else if ($format == 'date' AND $value != '' AND !preg_match("/^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))$/", $value)) // 2021-08-31
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une date valide");
		else if ($format == 'datetime' AND !preg_match("/^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))\s(([01]\d|2[0-3]):([0-5]\d):([0-5]\d)(.(\d{1,12}))?)$/", $value)) // 2020-02-29 00:00:00 + optional milliseconds .000000000000
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une date/heure valide");
		else if ($format == 'time' AND !preg_match("/^(([01]\d|2[0-3]):([0-5]\d):([0-5]\d))$/", $value)) // 00:00:00
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une heure valide");
		else if ($format == 'resource' AND $value!='*' AND !preg_match("/^[a-z0-9@\.\/]+\*?$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une ressource valide");
		else if ($format == 'module' AND !preg_match("/^[a-z0-9-_\/]+$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un module valide");
		else if ($format == 'hexcolor' AND !preg_match("/^(?:[0-9a-fA-F]{3}){1,2}$/", $value))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une couleur hexadécimale valide");
		else if ($format == 'rrule')
		{
			try
			{
				require_once('api_optimus-server/libs/When/Valid.php');
				require_once('api_optimus-server/libs/When/When.php');
				$r = new When();
				$occurrences = $r->startDate(new DateTime('2000-01-01 00:00:00'))->rrule($value);
			}
			catch (Throwable $t)
			{
				$return = array("code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une règle rrule valable", "detail" => $t->getMessage());
			}
		}
		elseif ($format == 'password')
		{
			if (!preg_match('@[A-Z]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins une lettre majuscule");
			if (!preg_match('@[a-z]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins une lettre minuscule");
			if (!preg_match('@[0-9]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins un chiffre");
			if (!preg_match('@[^\w]@', $value)) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins un caractère spécial");
			if (strlen($value) < 9) 
				$return = array("code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins 9 caractères");
		}
		else if ($format == 'json')
		{
			json_decode($value);
			if (json_last_error() != JSON_ERROR_NONE)
				$return = array("code" => 400, "message" =>  $value . " : n'est pas une chaine JSON valide");
		}
		else if ($format == 'locked' AND isset($value))
			$return = array("code" => 400, "message" =>  $name . " : ne peut pas être modifié");
		
		else if (!in_array($format,array('topdomain','domain','email','email_list','origin','ipv4','integer','decimal','boolean','string','text','filename','date','datetime','time','resource','module','locked','password','json','hexcolor','rrule')))
			$return = array("code" => 400, "message" =>  $name . ' : ' . $format . " n'est pas un format valide");

			
	if ($required AND !isset($value))
		$return = array("code" => 400, "message" => $name . " doit obligatoirement être renseigné");

	if ($format == 'boolean')
		$input->fields[$name] = PDO::PARAM_BOOL;
	else if ($format == 'integer')
		$input->fields[$name] = PDO::PARAM_INT;
	else
		$input->fields[$name] = PDO::PARAM_STR;

	if (isset($value) AND $value == '')
		if (in_array($format, array('topdomain','domain','email','email_list','origin','ipv4','string','text','date','datetime','resource','module')))
			$input->body->$name = null;
		else if (in_array($format, array('integer','decimal','boolean')))
			$input->body->$name = 0;
		else if (in_array($format, array('filename','password','json')))
			$return = array("code" => 400, "message" => $name . " ne peut pas être vide");

	if (@$return)
	{
		http_response_code($return['code']);
		die(json_encode($return));
	}
	else
		return true;
}

function output($array)
{
	http_response_code($array['code']);
	die(json_encode($array));
}

function check($name, $value, $type, $required = false)
{
	global $input;
	//echo $name . ':' . $value . "\n";

	if ($required AND !isset($value))
		output(["code" => 400, "message" => $name . " doit obligatoirement être renseigné"]);

	if (!in_array($type,array('topdomain','domain','email','email_list','origin','ipv4','integer','positive_integer', 'negative_integer','decimal','boolean','string','text','filename','date','datetime','time','resource','module','locked','password','json','hexcolor','rrule')))
		output(["code" => 400, "message" =>  $name . ' : ' . $type . " n'est pas un format valide"]);
	
	if (preg_match("/^\d{1,2}\/\d{1,2}\/\d{4}$/", $value))
	{
		$value = explode('/', $value);
		$value =  $value[2] . '-' . str_pad($value[1],2,'0',STR_PAD_LEFT) . '-' .str_pad($value[0],2,'0',STR_PAD_LEFT);
	}

	if ($type == 'boolean' AND strtolower($value) == 'false')
		$value = 0;
	if ($type == 'boolean' AND strtolower($value) == 'true')
		$value = 1;
	

	if (isset($value) && isset($type))
		if ($type == 'topdomain' AND !preg_match("/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)([a-z0-9][a-z0-9-]*[a-z0-9])$/", $value)) // web.network
			output(["code" => 400, "message" => $name . ' : ' . $value . " n'est pas un domaine de premier niveau valide"]);
		else if ($type == 'domain' AND !preg_match("/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9])$/", $value)) // web.network  test.web.network
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un domaine valide"]);
		else if ($type == 'email' AND !preg_match("/^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]))$/", $value)) // test_6@test.network  test-8@web.test.network
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une adresse email valide"]);
		else if ($type == 'email_list' AND !preg_match("/^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]);?)+$/", $value)) // first@test.org;second@test.org;third@test.org
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une liste d'adresses email valide"]);
		else if ($type == 'origin' AND !preg_match("/^(\*)$|^(localhost|(\*\.)?([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z0-9][a-z0-9-]*[a-z0-9]|(2[0-4]\d|25[0-5]|1\d\d|\d\d?)\.(?5)\.(?5)\.(?5))(:((6553[0-5])|(655[0-2][0-9])|(65[0-4][0-9]{2})|(6[0-4][0-9]{3})|([1-5][0-9]{4})|([0-5]{0,5})|([0-9]{1,4})))?$/", $value)) // *  localhost:8080  *.optimus-avocats.fr  www.optimus-avocats.fr:9091  192.168.0.5:3232
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une origine valide"]);
		else if ($type == 'ipv4' AND !preg_match("/^(2[0-4]\d|25[0-5]|1\d\d|\d\d?)\.(?1)\.(?1)\.(?1)$/", $value)) // 192.168.0.5
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une adresse ip valide"]);
		else if ($type == 'integer' AND !preg_match("/^-?\d+$/", $value)) // 1215 -4 0 2
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier valide"]);
		else if ($type == 'positive_integer' AND !preg_match("/^\d+$/", $value)) // 0 1215
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier positif valide"]);
		else if ($type == 'negative_integer' AND !preg_match("/^-\d+$/", $value)) // -1215 0
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier négatif valide"]);
		else if ($type == 'strictly_positive_integer' AND !preg_match("/^[1-9]\d*$/", $value)) // 1215
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier strictement positif valide"]);
		else if ($type == 'strictly_negative_integer' AND !preg_match("/^-[1-9]\d*$/", $value)) // -1215
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre entier strictement négatif valide"]);
		else if ($type == 'decimal' AND !preg_match("/^-?(\d*\.)?\d+$/", $value)) // 44.59
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre decimal valide"]);
		else if ($type == 'boolean' AND !preg_match("/^[01]$/", $value))
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nombre boolean valide. Valeurs autorisées : 0 ou 1"]);
		else if ($type == 'string' AND !preg_match("/^[\w\t \-œàâäéèêëïîôöùûüÿç().,?!\"\'+=+\-=\£\$\€\/_*@&~#%:;°§\[\]\{\}]+$/D", $value))
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une chaîne valide"]);
		else if ($type == 'text')
		{
			foreach(explode("\n", $value) as $line)
				if(!preg_match("/^[\w\t \-œàâäéèêëïîôöùûüÿç().,?!\"\'+=+\-=\£\$\€\/_*@&~#%:;°§\[\]\{\}]+$/D", $line))
					output(["code" => 400, "message" =>  $name . ' : ' . $line . " n'est pas une chaîne valide"]);
		}
		else if ($type == 'filename' AND ($value=='.' OR $value=='..' OR substr($value,0,1)==' ' OR substr($value,-1)==' ' OR !preg_match("/^[a-zA-Z0-9 ._@\-àâäéèêëïîôöùûüÿç()\']+$/", $value)))
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un nom de fichier valide"]);
		else if ($type == 'date' AND $value != '' AND !preg_match("/^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))$/", $value)) // 2021-08-31
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une date valide"]);
		else if ($type == 'datetime' AND !preg_match("/^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))\s(([01]\d|2[0-3]):([0-5]\d):([0-5]\d)(.(\d{1,12}))?)$/", $value)) // 2020-02-29 00:00:00 + optional milliseconds .000000000000
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une date/heure valide"]);
		else if ($type == 'time' AND !preg_match("/^(([01]\d|2[0-3]):([0-5]\d):([0-5]\d))$/", $value)) // 00:00:00
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une heure valide"]);
		else if ($type == 'resource' AND !preg_match("/^[a-z-_\/]{2,64}(\/\d{1,16})?$|^\*$/", $value))
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une ressource valide"]);
		else if ($type == 'module' AND !preg_match("/^[a-z0-9-_]+$/", $value))
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas un module valide"]);
		else if ($type == 'hexcolor' AND !preg_match("/^(?:[0-9a-fA-F]{3}){1,2}$/", $value))
			output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une couleur hexadécimale valide"]);
		else if ($type == 'rrule')
		{
			if (stripos($value, 'FREQ=') === false)
				output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une règle rrule valable", "detail" => "La règle ne contient pas le paramètre FREQ qui est obligatoirement requis"]);
			else try
			{
				require_once('api_optimus-server/libs/When/Valid.php');
				require_once('api_optimus-server/libs/When/When.php');
				$r = new When();
				$occurrences = $r->startDate(new DateTime('2000-01-01 00:00:00'))->rrule($value);
			}
			catch (Throwable $t)
			{
				output(["code" => 400, "message" =>  $name . ' : ' . $value . " n'est pas une règle rrule valable", "detail" => $t->getMessage()]);
			}
		}
		elseif ($type == 'password')
		{
			if (!preg_match('@[A-Z]@', $value)) 
				output(["code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins une lettre majuscule"]);
			if (!preg_match('@[a-z]@', $value)) 
				output(["code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins une lettre minuscule"]);
			if (!preg_match('@[0-9]@', $value)) 
				output(["code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins un chiffre"]);
			if (!preg_match('@[^\w]@', $value)) 
				output(["code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins un caractère spécial"]);
			if (strlen($value) < 9) 
				output(["code" => 400, "message" =>  $name . " : Le mot de passe doit contenir au moins 9 caractères"]);
		}
		else if ($type == 'json')
		{
			json_decode($value);
			if (json_last_error() != JSON_ERROR_NONE)
				output(["code" => 400, "message" =>  $name . " : n'est pas une chaine JSON valide"]);
		}

		return $value;
}

function check_input_body($resource, $method)
{
	global $input;

	$resource = (object)array_filter((array)$resource, function($column){return (!$column->reference AND !$column->virtual);});

	foreach ($input->body as $key => $value)
		if (!$resource->$key)
			unset($input->body->$key);

	foreach ($resource as $key => $value)
	{


		if (in_array('required', $resource->$key->$method) AND !array_key_exists($key, get_defined_vars()['input']->body))
			output(["code" => 400, "message" => $key . " doit obligatoirement être renseigné"]);
		else if (in_array('notnull', $resource->$key->$method) && array_key_exists($key, get_defined_vars()['input']->body) && $input->body->$key === null)
			output(["code" => 400, "message" => $key . " ne peut pas avoir la valeur 'null'"]);
		else if (in_array('notempty', $resource->$key->$method) && $input->body->$key === '')
			output(["code" => 400, "message" => $key . " ne peut pas être une chaine vide"]);

		if (in_array('nulltoempty', $resource->$key->$method) AND $input->body->$key === null)
			$input->body->$key = (string)'';

		if (in_array('undefinedtodefault', $resource->$key->$method) AND isset($resource->$key->default) AND !array_key_exists($key, get_defined_vars()['input']->body))
			$input->body->$key = $resource->$key->default;

		if (in_array('emptytodefault', $resource->$key->$method) AND isset($resource->$key->default) AND $input->body->$key === '')
			$input->body->$key = $resource->$key->default;
		
		if (in_array('nulltodefault', $resource->$key->$method) AND isset($resource->$key->default) AND in_array('nulltodefault', $resource->$key->$method) AND $input->body->$key === null)
			$input->body->$key = $resource->$key->default;
		
		if (in_array('emptytonull', $resource->$key->$method) AND $input->body->$key === '')
			$input->body->$key = null;

		if ($input->body->$key === 'current_user')
			$input->body->$key = $input->user->id;

		if ($input->body->$key != null AND $input->body->$key != '')
			$input->body->$key = check($key, $input->body->$key, $resource->$key->type);
		
		if (in_array('emptytonull', $resource->$key->$method) AND $input->body->$key === '')
			if (in_array($resource->$key->type, array('integer','decimal','boolean')))
				$input->body->$key = 0;
			else
				$input->body->$key = null;

		

		if (array_key_exists($key, get_defined_vars()['input']->body) && !in_array('ignored', $resource->$key->$method))
			if ($resource->$key->type == 'boolean')
				$input->pdotype->$key = PDO::PARAM_BOOL;
			else if ($resource->$key->type == 'integer')
				$input->pdotype->$key = PDO::PARAM_INT;
			else 
				$input->pdotype->$key = PDO::PARAM_STR;

						
	}



	if ($method == 'patch' && !$input->pdotype)
		output(["code" => 400, "message" => "Aucune donnée à modifier n'a été transmise"]);

	return true;
}

function sanitize($resource, $array)
{
	if (is_array($array))
		foreach($array as $subkey => $subarray)
			$array[$subkey] = sanitize($resource, $subarray);

	foreach($array as $key => $value)
		if ($resource->$key->type == 'integer')
			$array[$key] = intval($value);
		else if ($resource->$key->type == 'decimal')
			$array[$key] = floatval($value);
		else if ($resource->$key->type == 'boolean')
			$array[$key] = boolval($value);
	return $array;
}

//DEPRECATED
function get_rights($user_id, $owner_id, $resource)
{
	global $connection;
	if ($user_id === $owner_id OR is_admin($user_id))
		return array('read' => true, 'write' => true, 'create' => true, 'delete' => true);

	$resources_array = explode('/',  $resource);
	while (sizeof($resources_array) > 0)
	{
		$resources[] = "'" . implode('/',$resources_array) . "'";
		array_pop($resources_array);
	}
	$resources[] = "'*'";
	
	$authorizations = $connection->prepare('SELECT `read`, `write`, `create`, `delete` FROM `server`.`authorizations` WHERE user = :user AND owner = :owner AND resource IN (' . implode(',',$resources) . ') ORDER BY length(resource) DESC');
	$authorizations->bindParam(':user', $user_id, PDO::PARAM_INT);
	$authorizations->bindParam(':owner', $owner_id, PDO::PARAM_INT);
	$authorizations->execute();

	if ($authorizations->rowCount() == 0)
		return array('read' => false, 'write' => false, 'create' => false, 'delete' => false);
	else
	{
		$authorizations = $authorizations->fetch(PDO::FETCH_ASSOC);
		$authorizations['read'] = boolval($authorizations['read']);
		$authorizations['write'] = boolval($authorizations['write']);
		$authorizations['delete'] = boolval($authorizations['delete']);
		$authorizations['create'] = boolval($authorizations['create']);
		return $authorizations;
	}
}

function get_restrictions($user_id, $owner_id, $resource)
{
	global $connection;
	if ($user_id === $owner_id OR is_admin($user_id))
		return array();

	$resources_array = explode('/',  $resource);
	while (sizeof($resources_array) > 0)
	{
		$resources[] = "'" . implode('/',$resources_array) . "'";
		array_pop($resources_array);
	}
	$resources[] = "'*'";
		
	if (strpos($resource, '/'))
		$authorizations = $connection->prepare('SELECT `read`, `write`, `delete` FROM `server`.`authorizations` WHERE user = :user AND owner = :owner AND resource IN (' . implode(',',$resources) . ') ORDER BY length(resource) DESC');
	else
		$authorizations = $connection->prepare('SELECT `read`, `write`, `create`, `delete` FROM `server`.`authorizations` WHERE user = :user AND owner = :owner AND resource IN (' . implode(',',$resources) . ') ORDER BY length(resource) DESC');
	$authorizations->bindParam(':user', $user_id, PDO::PARAM_INT);
	$authorizations->bindParam(':owner', $owner_id, PDO::PARAM_INT);
	$authorizations->execute();

	if ($authorizations->rowCount() > 0)
		$authorizations = array_keys($authorizations->fetch(PDO::FETCH_ASSOC), '0', true);
	else
		$authorizations =  array('read', 'write', 'create', 'delete');
	
	return $authorizations;
}

function get_restrictions_list($user_id, $owner_id, $resource)
{
	global $connection;
	
	$parent_restrictions = $connection->prepare("SELECT `read`, `write`, `create`, `delete` FROM `server`.`authorizations` WHERE user = :user AND owner = :owner AND (resource = '*' OR resource = :resource) ORDER BY length(resource) DESC");
	$parent_restrictions->bindParam(':user', $user_id, PDO::PARAM_INT);
	$parent_restrictions->bindParam(':owner', $owner_id, PDO::PARAM_INT);
	$parent_restrictions->bindParam(':resource', $resource, PDO::PARAM_STR);
	$parent_restrictions->execute();
	if ($parent_restrictions->rowCount() > 0)
		$response[$resource] = array_keys($parent_restrictions->fetch(PDO::FETCH_ASSOC), '0', true);
	else
		$response[$resource] = array('read', 'write', 'create', 'delete');
	

	$children_restrictions = $connection->prepare('SELECT `resource`, `read`, `write`, `delete` FROM `server`.`authorizations` WHERE user = :user AND owner = :owner AND resource LIKE :resource');
	$children_restrictions->bindParam(':user', $user_id, PDO::PARAM_INT);
	$children_restrictions->bindParam(':owner', $owner_id, PDO::PARAM_INT);
	$child_resource = $resource . "/%";
	$children_restrictions->bindParam(':resource', $child_resource, PDO::PARAM_STR);
	$children_restrictions->execute();
	$children_restrictions = $children_restrictions->fetchAll(PDO::FETCH_ASSOC);
	foreach($children_restrictions as $restrictions)
		$response[$restrictions['resource']] = array_keys($restrictions, '0', true);
	return $response;
}

function get_user_db($user_id)
{
	global $connection;
	$db = $connection->query("SELECT `email` FROM `server`.`users` WHERE id = '" . $user_id . "'")->fetch(PDO::FETCH_ASSOC);
	if (!$db['email'])
	{
		http_response_code(404);
		die(json_encode(array("code" => 404, "message" =>  "L'utilisateur " . $user_id . " n'existe pas")));
	}
	return $db['email'];
}

function get_user_id($user_db)
{
	global $connection;
	$db = $connection->query("SELECT `id` FROM `server`.`users` WHERE email = '" . $user_db . "'")->fetch(PDO::FETCH_ASSOC);
	if (!$db['id'])
	{
		http_response_code(404);
		die(json_encode(array("code" => 404, "message" =>  "L'utilisateur " . $user_db . " n'existe pas")));
	}
	return $db['id'];
}
?>