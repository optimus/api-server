<?php

function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$banned_ips = `sudo /bin/fail2ban-client banned`;
	$banned_ips = str_replace("'", '"', $banned_ips);
	$banned_ips = json_decode($banned_ips, true);

	return array("code" => 200, "data" => $banned_ips);
}

function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	validate('ip', $input->body->ip, 'ipv4', false);
	$unbanned = shell_exec("sudo /bin/fail2ban-client unban " . $input->body->ip);

	if ($unbanned == 1)
		return array("code" => 200, "message" => 'IP ' . $input->body->ip . ' has been removed from banlist');
	else if ($unbanned == 0)
		return array("code" => 200, "message" => 'IP ' . $input->body->ip . ' is not listed in any banlist');
	else
		return array("code" => 500, "message" => 'an unknown error has occured');
}
?>