<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$mailbox = $connection->query("SELECT id, domain FROM mailserver.mailboxes_domains ORDER BY id")->fetchAll(PDO::FETCH_ASSOC);
	return array("code" => 200, "data" => $mailbox);
}


function post()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	validate('domain', $input->body->domain, 'topdomain', true);
	
	if (exists($connection, 'mailserver', 'mailboxes_domains', 'domain', $input->body->domain))
		return array("code" => 409, "message" => "Erreur - Ce domaine existe déjà");

	$next_id = $connection->query("SELECT MAX(id) FROM `mailserver`.`mailboxes_domains`")->fetch(PDO::FETCH_ASSOC);

	$mailbox = $connection->prepare("INSERT INTO mailserver.mailboxes_domains SET id = '" . ($next_id['MAX(id)']+1) . "', status = 1, domain = '" . $input->body->domain . "'");
	if($mailbox->execute())
	{
		exec('sudo /usr/sbin/postfix reload');
		return array("code" => 201, "data" => array('id' => ($next_id['MAX(id)']+1), 'domain' => strtolower($input->body->domain)), "message" => "Domaine ajouté avec succès");
	}
	else
		return array("code" => 400, "message" => $mailbox->errorInfo()[2]);
}


function patch()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = $input->path[2];
	validate('id', $input->path[2], 'integer', true);
	validate('domain', $input->body->domain, 'topdomain', true);

	if (exists($connection, 'mailserver', 'mailboxes_domains', 'domain', $input->body->domain))
		return array("code" => 409, "message" => "Erreur - Ce domaine existe déjà");

	$mailbox = $connection->prepare("UPDATE `mailserver`.`mailboxes_domains` SET domain = '" . strtolower($input->body->domain) . "' WHERE id = '" . $input->id . "'");
	if($mailbox->execute())
	{
		exec('sudo /usr/sbin/postfix reload');
		return array("code" => 200, "data" => array('id' => $input->id, 'domain' => strtolower($input->body->domain)), "message" => "Domaine ajouté avec succès");
	}
	else
		return array("code" => 400, "message" => $mailbox->errorInfo()[2]);
}


function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = $input->path[2];
	validate('id', $input->path[2], 'integer', true);
	
	if (!exists($connection, 'mailserver', 'mailboxes_domains', 'id', $input->id))
		return array("code" => 409, "message" => "Erreur - Ce domaine n'existe pas dans la base");
	
	$delete = $connection->query("DELETE FROM mailserver.mailboxes_domains WHERE id = '" . $input->id . "'");
	if($delete->execute())
	{
		exec('sudo /usr/sbin/postfix reload');
		return array("code" => 200, "message" => "Domaine supprimé avec succès");
	}
	else
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
}
?>
