<?php
function get()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou l'utilisteur lui même peuvent accéder à cette information");

	$servers = $connection->query("SELECT user, server FROM `server`.`users_servers` ORDER BY server")->fetchAll(PDO::FETCH_ASSOC);
	return array("code" => 200, "data" => $servers);
}


function post()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];
	validate('server', $input->body->server, 'topdomain', true);

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou l'utilisteur lui même peuvent accéder à cette information");
	
	$server_exists = $connection->prepare("SELECT * FROM `server`.`users_servers` WHERE user = :user AND server = :server");
	$server_exists->bindParam(':user', $input->id, PDO::PARAM_INT);
	$server_exists->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	$server_exists->execute();
	if ($server_exists->rowCount() > 0)
		return array("code" => 409, "message" => "Erreur - Ce serveur existe déjà");

	$server = $connection->prepare("INSERT INTO `server`.`users_servers` SET  user = :user, server = :server");
	$server->bindParam(':user', $input->id, PDO::PARAM_INT);
	$server->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	if($server->execute())
		return array("code" => 201,  "data" => array("server" => $input->body->server));
	else
		return array("code" => 400, "message" => $server->errorInfo()[2]);
}


function patch()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];
	validate('server', $input->body->server, 'topdomain', true);
	validate('oldvalue', $input->body->oldvalue, 'topdomain', true);

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou l'utilisteur lui même peuvent accéder à cette information");

	$server_exists = $connection->prepare("SELECT * FROM `server`.`users_servers` WHERE user = :user AND server = :server");
	$server_exists->bindParam(':user', $input->id, PDO::PARAM_INT);
	$server_exists->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	$server_exists->execute();
	if ($server_exists->rowCount() > 0)
		return array("code" => 409, "message" => "Erreur - Ce serveur existe déjà");

	$server = $connection->prepare("UPDATE `server`.`users_servers` SET server = :server WHERE user = :user AND server = :oldvalue");
	$server->bindParam(':user', $input->id, PDO::PARAM_INT);
	$server->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	$server->bindParam(':oldvalue', $input->body->oldvalue, PDO::PARAM_STR);
	if($server->execute())
		return array("code" => 200);
	else
		return array("code" => 400, "message" => $server->errorInfo()[2]);
}


function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	validate('id', $input->path[2], 'integer', true);
	$input->id = $input->path[2];
	validate('server', $input->body->server, 'topdomain', true);

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou l'utilisteur lui même peuvent accéder à cette information");

	$server_exists = $connection->prepare("SELECT * FROM `server`.`users_servers` WHERE user = :user AND server = :server");
	$server_exists->bindParam(':user', $input->id, PDO::PARAM_INT);
	$server_exists->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	$server_exists->execute();
	if ($server_exists->rowCount() == 0)
		return array("code" => 409, "message" => "Erreur - Ce domaine n'existe pas dans la base");
	
	$delete = $connection->prepare("DELETE FROM `server`.`users_servers` WHERE user = :user AND server = :server");
	$delete->bindParam(':user', $input->id, PDO::PARAM_INT);
	$delete->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	if($delete->execute())
		return array("code" => 200, "message" => "Domaine supprimé avec succès");
	else
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
}
?>
