<?php
use optimus\JWT\JWT;
function post()
{
	global $connection, $input, $sha_key, $aes_key, $domain;
	allowed_origins_only();

	validate('email',$input->body->email,'email',true);

	$exists = $connection->prepare("SELECT id, admin, status, email, password, firstname, lastname, CASE WHEN COALESCE(firstname,lastname,'') = '' THEN email ELSE TRIM(CONCAT(COALESCE(firstname,''),' ',COALESCE(lastname,''))) END as displayname FROM `server`.`users` WHERE email = :email AND status = 1 LIMIT 0,1");
	$exists->bindParam(':email', $input->body->email, PDO::PARAM_STR);
	$exists->execute();
	if($exists->rowCount() != 1)
		 return array("code" => 401, "message" => "Accès refusé - Utilisateur inconnu");

	$user = $exists->fetch(PDO::FETCH_OBJ);

	if (openssl_encrypt($input->body->password, 'aes-128-ecb', $aes_key) == base64_encode($user->password))
	{
		$jwt = new JWT($sha_key, 'HS512', 14400, 10);
		$token = $jwt->encode(["user" => array("id" => $user->id, "email" => $user->email), "aud" => "http://$domain", "scopes" => ['user'], "iss" => "http://$domain"]);
		$cookie_options = array ('expires' => time() + 14400, 'path' => '/', 'domain' => $domain, 'secure' => true, 'httponly' => true, 'samesite' => 'None');
		setcookie('token', $token, $cookie_options);
		return array("code" => 200, "data" => array("id" => $user->id, "status" => $user->status, "admin" => $user->admin, "email" => $user->email, "firstname" => $user->firstname, "lastname" => $user->lastname, "displayname" => $user->displayname), "message" => "Authentification réussie");
	}
	else
		return array("code" => 401, "message" => "Accès refusé - Mot de passe erroné");
}
?>
