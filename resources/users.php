<?php
include_once 'api_optimus-server/datatables.php';
$resource = json_decode('
{
	"id": { "type": "integer", "field": "users.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"status": { "type": "boolean", "field": "users.status", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"admin": { "type": "boolean", "field": "users.admin", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"email": { "type": "email", "field": "users.email", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": "" },
	"firstname": { "type": "string", "field": "users.firstname", "default": 0 },
	"lastname": { "type": "string", "field": "users.lastname", "default": 0 },
	"password": { "type": "password", "field": "users.password", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"displayname" : { "type" : "string", "field": "users.displayname" }
}
', null, 512, JSON_THROW_ON_ERROR);

function get()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'integer', false);
	

	//REQUETE SUR UN UTILISATEUR IDENTIFIÉ
	if (isset($input->id))
	{
		$input->body = json_decode('{"references": true, "filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'server', 'users');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cet utilisateur n'existe pas");
		else
		{
			unset($results[0]['password']);
			return array("code" => 200, "data" => sanitize($resource, $results[0]));
		}
	}
	//REQUETE SUR TOUS LES UTILISATEURS AU FORMAT DATATABLES
	else 
	{	
		$results = datatable_request($connection, $resource, 'server', 'users');
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		for($i=0; $i < sizeof($results); $i++)
			unset($results[$i]['password']);
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
}

function post()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();
	admin_only();

	check_input_body($resource, __METHOD__);

	if (isset($input->body->email))
		$input->body->email = strtolower($input->body->email);

	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");
	
	include 'config.php';
	$input->body->password = base64_decode(openssl_encrypt($input->body->password, 'aes-128-ecb', $aes_key));

	$query = datatables_insert($connection, $resource, 'server', 'users');
	if($query->execute())
	{
		$new_id = $connection->lastInsertId();
		$newmailbox = $connection->query("INSERT INTO `mailserver`.`mailboxes` SET id = " . $new_id . ", email='" . $input->body->email . "', quota=0, status=1")->fetch(PDO::FETCH_ASSOC);
		umask(0);
		@mkdir('/srv/mailboxes/' . $input->body->email, 0770);
		@chmod('/srv/mailboxes/' . $input->body->email, 0770);
		@chgrp('/srv/mailboxes/' . $input->body->email, 'mailboxes');
		@touch('/srv/mailboxes/' . $input->body->email . '/subscriptions');
		@chmod('/srv/mailboxes/' . $input->body->email . '/subscriptions', 0770);
		@chgrp('/srv/mailboxes/' . $input->body->email . '/subscriptions', 'mailboxes');
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'server', 'users');
		unset($results[0]['password']);
		return array("code" => 201, "data" => sanitize($resource, $results[0]), "message" => "Utilisateur créé avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
}

function patch()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'integer', false);

	check_input_body($resource, __METHOD__);

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur ou l'utilisateur lui même peuvent modifier un utilisateur");

	if (isset($input->body->password))
	{
		include 'config.php';
		$input->body->password = base64_decode(openssl_encrypt($input->body->password, 'aes-128-ecb', $aes_key));
	}

	if (isset($input->body->email))
		$input->body->email = strtolower($input->body->email);

	if (!exists($connection, 'server','users', 'id', $input->id))
		return array("code" => 409, "message" => "Erreur - cet utilisateur n'existe pas");
	
	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");
	
	$query = datatables_update($connection, $resource, 'server', 'users', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'server', 'users');
		unset($results[0]['password']);
		return array("code" => 200, "data" => sanitize($resource, $results[0]), "message" => "Utilisateur modifié avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
}

function delete()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'integer', false);
	$input->db = get_user_db($input->id);

	if (is_dir('/srv/files/' . $input->db))
		exec('rm -r /srv/files/' . $input->db);
	
	if (is_dir('/srv/mailboxes/' . $input->db))
		exec('rm -r /srv/mailboxes/' . $input->db);
	
	$delete = $connection->prepare("DELETE FROM `server`.`authorizations` WHERE user = '" . $input->id . "' OR owner = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`preferences` WHERE user = '" . $input->id . "' OR owner = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `mailserver`.`mailboxes` WHERE id = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users` WHERE id = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users_services` WHERE user = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users_servers` WHERE user = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DROP DATABASE IF EXISTS `" . $input->db . "`");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	return array("code" => 200);
}
?>
