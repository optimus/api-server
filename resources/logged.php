<?php
function get()
{
	global $input;
	auth();
	return array("code" => 200, "message" => "Authentification réussie", "data" => array('id' => $input->user->id, 'email' => $input->user->email));
}
?>
