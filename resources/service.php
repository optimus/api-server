<?php
function get()
{
	auth();
	allowed_origins_only();
	$latest = file_get_contents('https://git.cybertron.fr/optimus/api-server/-/raw/master/VERSION');
	$latest = explode("\n",$latest);
	$current = file_get_contents('/srv/api/api_optimus-server/VERSION');
	$current = explode("\n",$current);
	$version = array ("name"=>"optimus-server", "current_date"=>intval($current[0]), "current_version"=>$current[1], "latest_date"=>intval($latest[0]), "latest_version"=>$latest[1]);
	return array("code" => 200, "data" => $version);
}

function post()
{
	global $connection, $username, $password;
	auth();
	allowed_origins_only();
	admin_only();

	$actual_version_date = intval(trim(file('/srv/api/api_optimus-server/VERSION')[0]));
	$available_version_date = intval(trim(file_get_contents('https://git.cybertron.fr/optimus/api-server/-/raw/master/VERSION')));

	if ($available_version_date > $actual_version_date)
	{
		exec('rm -R /srv/api/api_optimus-server');
		exec('mkdir /srv/api/api_optimus-server');
		exec('git clone https://git.cybertron.fr/optimus/api-server /srv/api/optimus-server/.');

		$new_version_date = intval(trim(file('/srv/api/api_optimus-server/VERSION')[0]));

		$errors = array();
		$sql_files = array_diff(scandir('/srv/api/api_optimus-server/sql'), array('..', '.'));
		foreach($sql_files as $sql_file)
			if (preg_replace("/[^0-9]/","",$sql_file) > $actual_version_date)
				exec("mariadb -f -u" . $username . " -p" . $password . " < '/srv/api/api_optimus-server/sql/" . $sql_file . "' 2>&1", $errors);

		if (sizeof(@$errors)>0)
			return array("code" => 400, "message" => implode("\n",$errors));
		else
			return array("code" => 201, "data" => array("old_version"=>$actual_version_date, "new_version"=>$new_version_date), "message" => "Mise à jour effectuée avec succès");
	
		$connection->query("REPLACE INTO `server`.`services` SET `name` = 'optimus-server', `displayname` = 'OPTIMUS Server', `status` = 1, `description` = 'users - services - authorizations'");
	}
	else
		return array("code" => 200, "data" => array("actual_version"=>$actual_version_date, "available_version"=>$available_version_date), "message" => "Aucune mise à jour n'est disponible");
}